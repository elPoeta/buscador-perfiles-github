class GitHub {
    constructor() {
        this.client_id = '84221a0eeb5afce382b9';
        this.client_secret = '4ff4c700bd0484260e6d085df7c9c7a64d73fd75';
        this.repos = 5;
        this.repos_sort = 'created asc';
    }
    async getUsuario(usuario) {
        const perfilResponse = await fetch(`https://api.github.com/users/${usuario}?client_id=${this.client_id}&client_secret=${this.client_secret}`);

        const repoResponse = await fetch(`https://api.github.com/users/${usuario}/repos?per_page=${this.repos}&sort=${this.repos_sort}&${this.client_id}&client_secret=${this.client_secret}`);
        const perfil = await perfilResponse.json();
        const repos = await repoResponse.json();
        return {
            perfil,
            repos
        }
    }

}