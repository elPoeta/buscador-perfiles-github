const buscarUsuario = document.querySelector('#buscar-usuario');
const gitHub = new GitHub;
const ui = new UI;

buscarUsuario.addEventListener('keyup', e => buscar(e.target.value));

const buscar = texto => {
    if (texto !== '') {
        gitHub.getUsuario(texto).then(data => {
            if (data.perfil.message === 'Not Found') {
                ui.showAlerta('Perfil no encontrado', 'alert alert-danger');
            } else {
                ui.showPerfil(data.perfil);
                ui.showRepos(data.repos);
            }
        });
    } else {
        ui.limpiarPerfil();
    }
}