class UI {
    constructor() {
        this.perfil = document.querySelector('#perfil');
    }

    showPerfil(usuario) {
        this.limpiarAlerta();
        this.perfil.innerHTML = `<div class="card card-body mb-3>
        <div class="row">
        <div class="col-md-3">
        <img class="img-fluid mb-2" src="${usuario.avatar_url}">
        <a href="${usuario.html_url}" target="_blank" class="btn btn-primary btn-block mb-4">Ver Perfil</a>
        </div>
        <div class="col-md-9">
        <span class="badge badge-primary">Repos Publicos: ${usuario.public_repos}</span>
        <span class="badge badge-secondary">Gists Publicos: ${usuario.public_gists}</span>
        <span class="badge badge-success">Seguidores: ${usuario.followers}</span>
        <span class="badge badge-info">Siguiendo: ${usuario.following}</span>
        <br><br>
        <ul class="list-group">
        <li class="list-group-item">Compañía: ${usuario.company}</li>
        <li class="list-group-item">Website/Blog: ${usuario.blog}</li>
        <li class="list-group-item">Ubicacion: ${usuario.location}</li>
        <li class="list-group-item">Miembro desde: ${usuario.created_at}</li>
        </ul>
        </div>
        </div>
        </div>
        <h3 class="page-heading mb-3">Ultimos Repositorios</h3>
        <div id="repos"></div>`;
    }
    showRepos(repos) {
        let output = '';
        repos.forEach((repo) => {
            output += `<div class="card card-body mb2">
            <div class="row">
            <div class="col-md-6">
            <a href="${repo.html_url}" target="_blank">${repo.name}</a>
            </div>
            <div class="col-md-6">
            <span class="badge badge-primary">Stars: ${repo.stargazers_count}</span>
        <span class="badge badge-secondary">Watchers: ${repo.watchers_count}</span>
        <span class="badge badge-success">Forks: ${repo.forks_count}</span>
            </div>
            
            </div>
            </div>
            `;
        });

        document.querySelector('#repos').innerHTML = output;

    }
    limpiarPerfil() {
        this.perfil.innerHTML = '';
        this.limpiarAlerta();
    }

    showAlerta(msg, className) {
        this.limpiarAlerta();
        const div = document.createElement('div');
        const container = document.querySelector('.searchContainer');
        const buscar = document.querySelector('.search');
        div.className = className;
        div.appendChild(document.createTextNode(msg));
        container.insertBefore(div, buscar);
    }

    limpiarAlerta() {
        const currentAlert = document.querySelector('.alert');
        if (currentAlert) {
            currentAlert.remove();
        }
    }
}